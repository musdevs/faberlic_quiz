CREATE TABLE magazine (
  id INT NOT NULL AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL,
  PRIMARY KEY ($id),
  INDEX magazine_name (name)
);

CREATE TABLE user (
  id INT NOT NULL AUTO_INCREMENT,
  name VARCHAR(255),
  birthday DATE,
  PRIMARY KEY ($id),
  INDEX user_birthday (birthday)
);

CREATE TABLE subscribe (
  id INT NOT NULL AUTO_INCREMENT,
  magazine_id INT,
  user_id INT,
  PRIMARY KEY ($id),
  INDEX subscribe_user_id (user_id)
);

SELECT u.name
  FROM user u
  INNER JOIN subscribe s ON u.id = s.user_id
  INNER JOIN magazine m ON m.id = s.magazine_id
  WHERE
    m.name = 'Мурзилка'
    AND u.birthday < DATE_ADD(NOW(), INTERVAL -30 YEAR);

SELECT u.name
  FROM user u
  ORDER BY RAND()
  LIMIT 1;