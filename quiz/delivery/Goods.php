<?php

namespace Faberlic\Quiz\Delivery;


class Goods
{
    /**
     * @var float вес груза
     */
    protected $weight;

    /**
     * Goods constructor.
     * @param float $weight вес груза
     * @throws \Exception
     */
    public function __construct($weight)
    {
        if (empty($weight)) {
            throw new \Exception('У груза не может быть нулевой вес');
        }
        $this->weight = $weight;
    }

    /**
     * Возвращает вес груза
     * @return float
     */
    public function getWeight()
    {
        return $this->weight;
    }
}