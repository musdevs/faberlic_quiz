<?php

namespace Faberlic\Quiz\Delivery;

interface Calculable
{
    /**
     * Возвращаяет стоимость доставки груза
     *
     * @param Faberlic\Quiz\Goods $goods груз
     * @return float
     */
    public function getCost($goods);
}