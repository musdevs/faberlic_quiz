<?php

namespace Faberlic\Quiz\Delivery;

/**
 * DHLPostCalculator калькулятор доставки DHL
 */
class DHLCalculator implements Calculable
{
    /**
     * @var float цена доставки 1 кг
     */
    protected $price;

    /**
     * DHLPostCalculator constructor.
     * @param $price float цена доставки 1 кг
     */
    public function __construct($price)
    {
        $this->price = $price;
    }

    /**
     * @inheritdoc
     */
    public function getCost($goods)
    {
        return $goods->getWeight() * $this->price;
    }
}