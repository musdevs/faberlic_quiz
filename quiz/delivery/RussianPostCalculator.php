<?php

namespace Faberlic\Quiz\Delivery;

/**
 * RussianPostCalculator калькулятор доставки Почтой России
 */
class RussianPostCalculator implements Calculable
{
    /**
     * @var float предел небольшого веса
     */
    protected $smallWeightLimit;

    /**
     * @var float цена доставки небольшого веса
     */
    protected $smallPrice;

    /**
     * @var float цена доставки большого веса
     */
    protected $bigPrice;

    public function __construct($smallWeightLimit, $smallPrice, $bigPrice)
    {
        $this->smallWeightLimit = $smallWeightLimit;
        $this->smallPrice = $smallPrice;
        $this->bigPrice = $bigPrice;
    }

    /**
     * @inheritdoc
     */
    public function getCost($goods)
    {
        if ($goods->getWeight() < $this->smallWeightLimit) {
            return $this->smallPrice;
        } else {
            return $this->bigPrice;
        }
    }
}