<?php

spl_autoload_register(function ($className) {
    $parts = explode('\\', $className);
    require end($parts) . '.php';
});

use Faberlic\Quiz\Delivery\Goods;
use Faberlic\Quiz\Delivery\RussianPostCalculator;
use Faberlic\Quiz\Delivery\DHLCalculator;


$goods1 = new Goods(5);
$goods2 = new Goods(20);

$calc = new RussianPostCalculator(10, 100, 1000);
echo 'Почта России ' . $goods1->getWeight() . ': ' . $calc->getCost($goods1) . PHP_EOL;
echo 'Почта России ' . $goods2->getWeight() . ': ' . $calc->getCost($goods2) . PHP_EOL;

$calc = new DHLCalculator(100);
echo 'DHL ' . $goods1->getWeight() . ': ' . $calc->getCost($goods1) . PHP_EOL;
echo 'DHL ' . $goods2->getWeight() . ': ' . $calc->getCost($goods2) . PHP_EOL;